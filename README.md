# StateWalker
StateWalker is an easy to use flow engine based on Finite State Machine(FSM) for Java.  
The purpose of this project is making a flow system can be designed and implemented easily, and make sure the FSM model can be executed with high performance. 

---
## Installation
1. Setup repository registry in `pom.xml`
```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/17557179/packages/maven</url>
    </repository>
</repositories>

<distributionManagement>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/17557179/packages/maven</url>
    </repository>
    <snapshotRepository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/17557179/packages/maven</url>
    </snapshotRepository>
</distributionManagement>
```
2. Add dependency in your `pom.xml`
```xml
<dependency>
    <groupId>io.qstudio</groupId>
    <artifactId>statewalker</artifactId>
    <version>0.4</version>
</dependency>
```

---
## Finite State Machine Concept of StateWalker
StateWalker provides `State` and `Transition` to build a FSM model which can be described as follows:
```
State(S) -- Transition(T) -> State(S')
```
Each `State` and `Transition` can implement different behavior on demand.

A FSM model is desiged to accomplish some `Task` which carries specific `Content` and needs an engine to run it.  
So, we designed `Walker` which execute tasks from state to state and `WalkerRunner` to run multiple walkers automatically and asynchronously.

---
### Define a State
#### By Implementation
A `State` can be created by implements `io.qstudio.statewalker.State`.  
There are three life cycle APIs:
 - `onEnterState` - optional
 - `onState`
 - `onExitState` - optional
```java
private static class Red implements State<Light> {

    @Override
    public Result onState(Task<Light> task) {
        if (!"Red".equals(task.getContent().getColor())) {
            task.getContent().show("Red", 10);
            return Result.sleep(10000);
        }
        if (task.getContent().isBusy()) {
            System.err.println("Still Busy => " + task.getContent());
        }
        return Result.exit("DONE");
    }
}
```

#### By Declaration
A `State` can be created by declaration style API.
```java
var red = StateBuilder.<Light>newBuilder()
    .setCode("Red")
    .setOnState((task) -> {
        if (!"Red".equals(task.getContent().getColor())) {
            task.getContent().show("Red", 10);
            return Result.sleep(10000);
        }
        if (task.getContent().isBusy()) {
            System.err.println("Still Busy => " + task.getContent());
        }
        return Result.exit("DONE");
    })
    .build();
```

#### About State.Result
There are 5 APIs can be used when implements the logic of `onState` task operation.
- `exit(String exitCode)` - Ready to transit to next state. This task will be added to task queue automatically.
  - The `exitCode` is the code of the transition which start from this state.
- `stay()` - Stay at the same state. This task will be added to task queue automatically.
- `suspend()` - Suspend and stay on the same state. This task can be resumed manually.
- `complete()` - Task is completed.
- `sleep(long sleepTime)` - Sleep for the specific time period.

---
### Define a Transition
#### By Implementation
A `Transition` can be created by implements `io.qstudio.statewalker.Transition`.  
There is optional life cycle api `onTransition` can be implemented.
```java
private static class R2Y implements Transition<Light> {

    @Override
    String getCode() {
        return "DONE";
    }

    @Override
    public String fromState() {
        return "Red";
    }

    @Override
    public String toState() {
        return "Yellow";
    }
}
```
#### By Declaration
A `Transition` can be created by declaration style API.
```java
var r2y = TransitionBuilder.<Light>newBuilder()
    .setCode("DONE")
    .setFromState("Red").setToState("Yellow")
    .build();
```

---
### Build a Walker
If you want to control state transition **by yourself**, you can build a walker and use it manually.

A walker to be build needs providing it's states and transitions.
```java
var walker = WalkerBuilder.<Light>newBuilder()
    .addStates(red, yellow, green)
    .addTransitions(r2y, y2g, g2r)
    .build();
```
You can also define a state or transition during building a walker using these APIs.
- `addState(String code, Function<Task<C>, Result> onState)`
- `addState(String code, Function<Task<C>, Result> onState, Consumer<Task<C>> onEnterState, Consumer<Task<C>> onExitState)`
- `addTransition(String code, String fromState, String toState)`
- `addTransition(String code, String fromState, String toState, Consumer<Task<C>> onTransition)`

#### Add a Task
Add a task to task queue by invoking `addTask`.
```java
walker.addTask(new Light(), "Red");
```
Execute first task in the quere by invoking `doTask`.   
`doTask` exeute the life cycle api from the task's current state to next state, then determine what to do next based on the logic implemented.
```java
var task = walker.doTask().get();
```

#### About TaskListener
`TaskListener` has 3 APIs which invoked by a walker when task achieved the status.
- `onComplete`
- `onError` - optional
- `onSuspend` - optional
- `onSleep` - optonal

To use `TaskListener` you need provide them when building a walker.
```java
var walker = WalkerBuilder.<Light>newBuilder()
    .addStates(red, yellow, green)
    .addTransitions(r2y, y2g, g2r)
    .addListeners(listener1, listener2)
    .build();
```
If you just need `onComplete` monitoring you can do this.
```java
var walker = WalkerBuilder.<Light>newBuilder()
    .addStates(red, yellow, green)
    .addTransitions(r2y, y2g, g2r)
    .addListeners(t -> lights.remove(t.getContent()))
    .build();
```

#### Trace the State Transition
Tracing state transition can be enable by three steps:
1. Add `@Action.Traced` on the life cycle API need to be traced
```java
public static class StateA implements State<Counter> {

    @Action.Traced
    @Override
    public State.Result onState(Task<Counter> task) {
        //...
    }
}
```
2. Build a action tracing walker
```java
var walker = WalkerBuilder.<Counter>newBuilder()
    .addStates(s1, s2)
    .addTransitions(t1, t2)
    .setEnableActionTracing(true)
    .build();
```
3. Get the actions after `doTask()`
```java
var t = walker.doTask().get();
var actions = t.getActions();
```

---
### Build a WalkerRunner
If you want to run state transition **automatically and asynchronously**, you can build a runner and just start it.
A WalkerRunner supports adding several different walkers into it.   
User can decide the amount of each walker and the thread pool size inside the runner. This is useful for optimizing the usage of memory and computing power.
To build a runner, you just need to add the walker into it.
```java
var runner = WalkerRunnerBuilder.<Light>newBuilder()
    .addWalker("DEFAULT", 3,
        WalkerBuilder.<Light>newBuilder()
            .addStates(new Red(), new Yellow(), new Green(), new Off())
            .addTransition("DONE", "Red", "Yellow")
            .addTransition("DONE", "Yellow", "Green")
            .addTransition("DONE", "Green", "Off")
        .build()) // "DEFAULT" is the walker code, the second argument "3" means there will be 3 walkers of this kind adding to this runner
    .setThreadPoolSize(3) // optional, default is 1
    .build();
```

#### Use a WalkerRunner
We provide these APIs to use a runner.
- `assignTask(String code, C content, String state)` - just like `addTask` of `Walker`, because of multiple kind walker supporting, you need to specify which walker the task to be added to.
- `start()` - start the runner
- `stop()` - stop the runner
- `isRunning()` - the runner started and still running
- `isBusy()` - the runner is running and has task to be completed.
- `getThreadPoolSize()` - the thread pool size of the runner
- `getWalkerCodes()` - the codes of different kind of walkers inside the runner

---
## Sample Code
Please take a look the samples and test cases at `test/java/` in this project.
