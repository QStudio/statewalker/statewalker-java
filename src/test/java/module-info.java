/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

/**
 * Defines the Finite State Machine(FSM) toolkit which is called StateWalker.
 * 
 * @author Arren Ping at QStudio.io
 * @since 0.1
 */
module qstudio.statewalker.test {
    requires qstudio.statewalker;
    requires org.junit.jupiter;
}
