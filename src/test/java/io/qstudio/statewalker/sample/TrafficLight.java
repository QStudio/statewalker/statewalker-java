/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.sample;

import io.qstudio.statewalker.Content;
import io.qstudio.statewalker.State;
import io.qstudio.statewalker.Task;
import io.qstudio.statewalker.WalkerBuilder;

/**
 * A simple traffic light sample using StateWalker.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public class TrafficLight {

    public static void main(String[] args) {
        var walker = WalkerBuilder.<Light>newBuilder()
            .addStates(new Red(), new Yellow(), new Green())
            .addTransition("DONE", "Red", "Yellow")
            .addTransition("DONE", "Yellow", "Green")
            .addTransition("DONE", "Green", "Red")
            .build();

        // Red
        walker.addTask(new Light(), "Red");
        var task = walker.doTask().get();
        System.out.println(task.getContent());
        System.out.println("======= AFTER " + task.getContent().getDuration() + " SECONDS =======");
        // Yellow
        task = walker.doTask().get();
        System.out.println(task.getContent());
        System.out.println("======= AFTER " + task.getContent().getDuration() + " SECONDS =======");
        // Green
        task = walker.doTask().get();
        System.out.println(task.getContent());
        System.out.println("======= AFTER " + task.getContent().getDuration() + " SECONDS =======");
        // back to Red
        task = walker.doTask().get();
        System.out.println(task.getContent());
    }

    private static class Light implements Content {

        private String color;

        private int duration;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        @Override
        public String toString() {
            return "Light{" + "color=" + color + ", duration=" + duration + '}';
        }
    }

    private static class Red implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            task.getContent().setColor("Red");
            task.getContent().setDuration(60);
            return Result.exit("DONE");
        }
    }

    private static class Yellow implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            task.getContent().setColor("Yellow");
            task.getContent().setDuration(5);
            return Result.exit("DONE");
        }
    }

    private static class Green implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            task.getContent().setColor("Green");
            task.getContent().setDuration(30);
            return Result.exit("DONE");
        }
    }
}
