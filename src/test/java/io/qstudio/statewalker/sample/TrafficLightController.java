/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.sample;

import io.qstudio.statewalker.Content;
import io.qstudio.statewalker.State;
import io.qstudio.statewalker.Task;
import io.qstudio.statewalker.WalkerBuilder;
import io.qstudio.statewalker.WalkerRunner;
import io.qstudio.statewalker.WalkerRunnerBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * A traffic light controller sample using StateWalker.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public class TrafficLightController {

    private static final LightController controller = new LightController();

    public static void startController() {
        controller.start();
    }

    public static void stopController() {
        controller.stop();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 8; i++) {
            controller.addLight(new Light(i));
        }
        startController();
        while (controller.isBusy()) {
            System.out.println("WAIT...");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
            }
        }
        stopController();
    }

    private static class Light implements Content {

        private final int id;

        private String color;

        private Integer duration;

        private Long startTime;

        public Light(int id) {
            this.id = id;
        }

        public String getColor() {
            return color;
        }

        public boolean isBusy() {
            if (startTime != null && duration != null) {
                return (System.currentTimeMillis() - startTime) < (duration * 1000);
            }
            return false;
        }

        public void show(String color, int duration) {
            this.color = color;
            this.duration = duration;
            this.startTime = System.currentTimeMillis();
            System.out.println(this);
        }

        public void turnOff() {
            color = null;
            duration = null;
            startTime = null;
        }

        @Override
        public String toString() {
            return "Light{" + "id=" + id + ", color=" + color + ", duration=" + duration + '}';
        }
    }

    private static class Red implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            if (!"Red".equals(task.getContent().getColor())) {
                task.getContent().show("Red", 10);
                return Result.sleep(10000);
            }
            if (task.getContent().isBusy()) {
                System.err.println("Still Busy => " + task.getContent());
            }
            return Result.exit("DONE");
        }
    }

    private static class Yellow implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            if (!"Yellow".equals(task.getContent().getColor())) {
                task.getContent().show("Yellow", 3);
                return Result.sleep(3000);
            }
            if (task.getContent().isBusy()) {
                System.err.println("Still Busy => " + task.getContent());
            }
            return Result.exit("DONE");
        }
    }

    private static class Green implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            if (!"Green".equals(task.getContent().getColor())) {
                task.getContent().show("Green", 7);
                return Result.sleep(7000);
            }
            if (task.getContent().isBusy()) {
                System.err.println("Still Busy => " + task.getContent());
            }
            return Result.exit("DONE");
        }
    }

    private static class Off implements State<Light> {

        @Override
        public Result onState(Task<Light> task) {
            task.getContent().turnOff();
            return Result.complete();
        }
    }

    private static class LightController {

        private final List<Light> lights = new ArrayList<>();

        private final WalkerRunner<Light> runner;

        public LightController() {
            runner = WalkerRunnerBuilder.<Light>newBuilder()
                .addWalker("DEFAULT", 3,
                    WalkerBuilder.<Light>newBuilder()
                        .addStates(new Red(), new Yellow(), new Green(), new Off())
                        .addTransition("DONE", "Red", "Yellow")
                        .addTransition("DONE", "Yellow", "Green")
                        .addTransition("DONE", "Green", "Off")
                        .build())
                .setThreadPoolSize(3)
                .build();
        }

        public void addLight(Light light) {
            lights.add(light);
            runner.assignTask("DEFAULT", light, "Red");
        }

        public List<Light> getLights() {
            return lights;
        }

        public void start() {
            runner.start();
        }

        public void stop() {
            runner.stop();
        }

        public boolean isBusy() {
            return runner.isBusy();
        }
    }
}
