/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.test;

import io.qstudio.statewalker.Content;
import io.qstudio.statewalker.State;
import io.qstudio.statewalker.StateBuilder;
import io.qstudio.statewalker.Walker;
import io.qstudio.statewalker.WalkerBuilder;
import io.qstudio.statewalker.WalkerRunnerBuilder;

import org.junit.jupiter.api.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test cases for WalkerRunner.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.3
 */
public class TestWalkerRunner {

    private State<Counter> A = StateBuilder.<Counter>newBuilder()
        .setCode("A")
        .setOnState(t -> {
            t.getContent().increase();
            return State.Result.exit("NEXT");
        })
        .build();

    private State<Counter> B = StateBuilder.<Counter>newBuilder()
        .setCode("B")
        .setOnState(t -> {
            t.getContent().increase();
            return State.Result.exit("NEXT");
        })
        .build();

    private State<Counter> END = StateBuilder.<Counter>newBuilder()
        .setCode("END")
        .setOnState(t -> State.Result.complete())
        .build();

    private Walker<Counter> walker1 = WalkerBuilder.<Counter>newBuilder()
        .addStates(A, B, END)
        .addTransition("NEXT", "A", "B")
        .addTransition("NEXT", "B", "END")
        .addListeners(t -> assertEquals(2, t.getContent().getCount()))
        .build();

    private Walker<Counter> walker2 = WalkerBuilder.<Counter>newBuilder()
        .addStates(A, B, END)
        .addTransition("NEXT", "B", "A")
        .addTransition("NEXT", "A", "END")
        .addListeners(t -> assertEquals(2, t.getContent().getCount()))
        .build();

    @Test
    public void testNoWalker() {
        assertThrows(IllegalArgumentException.class, () -> WalkerRunnerBuilder.newBuilder().build());
    }

    @Test
    public void testPoolSize() {
        var runner = WalkerRunnerBuilder.<Counter>newBuilder()
            .addWalker("A -> B", 0, walker1)
            .setThreadPoolSize(0)
            .build();
        assertEquals(1, runner.getThreadPoolSize());
        runner = WalkerRunnerBuilder.<Counter>newBuilder()
            .addWalker("A -> B", 2, walker1)
            .setThreadPoolSize(2)
            .build();
        assertEquals(2, runner.getThreadPoolSize());
    }

    @Test
    public void testRunner() {
        var code1 = "A -> B";
        var code2 = "B -> A";
        var runner = WalkerRunnerBuilder.<Counter>newBuilder()
            .addWalker(code1, 3, walker1)
            .addWalker(code2, 3, walker2)
            .setThreadPoolSize(3)
            .build();
        assertFalse(runner.isRunning());
        assertFalse(runner.isBusy());
        assertEquals(Set.of(code1, code2), runner.getWalkerCodes());
        var task = runner.assignTask(null, null, null);
        assertTrue(task.isEmpty());
        task = runner.assignTask(code1, new Counter(), "A");
        assertTrue(task.isPresent());
        runner.assignTask(code1, new Counter(), "A");
        runner.assignTask(code1, new Counter(), "A");
        runner.assignTask(code1, new Counter(), "A");
        runner.assignTask(code1, new Counter(), "A");
        runner.assignTask(code1, new Counter(), "A");
        runner.assignTask(code2, new Counter(), "B");
        runner.assignTask(code2, new Counter(), "B");
        runner.assignTask(code2, new Counter(), "B");
        runner.assignTask(code2, new Counter(), "B");
        runner.assignTask(code2, new Counter(), "B");
        assertTrue(runner.start());
        assertFalse(runner.start());
        assertTrue(runner.isRunning());
        assertTrue(runner.isBusy());
        while (runner.isBusy()) {
            System.out.println("Waiting for Runner...");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
            }
        }
        assertTrue(runner.stop());
        assertFalse(runner.stop());
    }

    public static class Counter implements Content {

        private int count;

        public void increase() {
            count++;
        }

        public int getCount() {
            return count;
        }

        @Override
        public String toString() {
            return "Counter{" + "count=" + count + '}';
        }
    }
}
