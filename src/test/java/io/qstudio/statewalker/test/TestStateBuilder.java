/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.test;

import io.qstudio.statewalker.State;
import io.qstudio.statewalker.StateBuilder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test cases for StateBuilder.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.3
 */
public class TestStateBuilder {

    @Test
    public void test() {
        assertThrows(NullPointerException.class, () -> StateBuilder.newBuilder().build());
        assertThrows(NullPointerException.class,
            () -> StateBuilder.newBuilder()
                .setCode("A")
                .build());
        var s1 = StateBuilder.newBuilder()
            .setCode("A")
            .setOnState((t) -> State.Result.complete())
            .build();
        assertNotNull(s1);
    }
}
