/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.test;

import io.qstudio.statewalker.TransitionBuilder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test cases for TransitionBuilder.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.3
 */
public class TestTransitionBuilder {

    @Test
    public void test() {
        assertThrows(NullPointerException.class, () -> TransitionBuilder.newBuilder().build());
        assertThrows(NullPointerException.class,
            () -> TransitionBuilder.newBuilder()
                .setFromState("A")
                .build());
        var t1 = TransitionBuilder.newBuilder()
            .setCode("DONE")
            .setFromState("A")
            .setToState("B")
            .build();
        assertNotNull(t1);
        assertEquals("DONE", t1.getCode());
        t1 = TransitionBuilder.newBuilder()
            .setFromState("A")
            .setToState("B")
            .setCode("C")
            .build();
        assertEquals("C", t1.getCode());
    }
}
