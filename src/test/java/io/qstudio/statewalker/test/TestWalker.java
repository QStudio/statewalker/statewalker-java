/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.test;

import io.qstudio.statewalker.Action;
import io.qstudio.statewalker.Content;
import io.qstudio.statewalker.State;
import io.qstudio.statewalker.StateBuilder;
import io.qstudio.statewalker.Task;
import io.qstudio.statewalker.TaskListener;
import io.qstudio.statewalker.Transition;
import io.qstudio.statewalker.TransitionBuilder;
import io.qstudio.statewalker.WalkerBuilder;
import io.qstudio.statewalker.exception.StateNotFoundException;
import io.qstudio.statewalker.exception.TransitionNotFoundException;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test cases for Walker.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.3
 */
public class TestWalker {

    @Test
    public void testNoState() {
        assertThrows(IllegalArgumentException.class, () -> WalkerBuilder.newBuilder().build());
    }

    @Test
    public void testNoTask() {
        var walker = WalkerBuilder.newBuilder()
            .addState("NONE", t -> State.Result.complete())
            .build();
        assertTrue(walker.doTask().isEmpty());
    }

    @Test
    public void testAddTask() {
        var walker = WalkerBuilder.newBuilder()
            .addState("NONE", t -> State.Result.complete())
            .build();
        walker.addTask(null, "A");
        assertEquals(1, walker.getTaskCount());
        walker.addTask(null, "A");
        assertEquals(2, walker.getTasks().size());
    }

    @Test
    public void testIsProcessing() {
        var walker = WalkerBuilder.newBuilder()
            .addState("A", t -> State.Result.complete())
            .build();
        assertFalse(walker.isProcessing());
        walker.addTask(null, "A");
        walker.doTask();
        assertFalse(walker.isProcessing());
    }

    @Test
    public void testNoCurrentState() {
        var walker = WalkerBuilder.newBuilder()
            .addState("NONE", t -> State.Result.complete())
            .build();
        walker.addTask(null, "A");
        var task = walker.doTask().get();
        assertEquals(Task.Status.ERROR, task.getStatus());
        assertTrue(task.getErrorCause().get() instanceof StateNotFoundException);
    }

    @Test
    public void testNoNextState() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.exit("DONE")).build();
        var t1 = TransitionBuilder.newBuilder().setCode("DONE").setFromState("A").setToState("B").build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).addTransitions(t1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        walker.doTask();
        assertEquals(Task.Status.ERROR, task.getStatus());
        assertTrue(task.getErrorCause().get() instanceof StateNotFoundException);
    }

    @Test
    public void testNoTransition() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.exit("DONE")).build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        walker.doTask();
        assertEquals(Task.Status.ERROR, task.getStatus());
        assertTrue(task.getErrorCause().get() instanceof TransitionNotFoundException);
    }

    @Test
    public void testResumeProcessingTask() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.stay()).build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        assertEquals(Task.Status.PROCESSING, task.getStatus());
        assertFalse(walker.resumeTask(task));
    }

    @Test
    public void testResumeErrorTask() {
        var walker = WalkerBuilder.newBuilder()
            .addState("NONE", t -> State.Result.complete())
            .build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        assertEquals(Task.Status.ERROR, task.getStatus());
        assertEquals(0, walker.getTaskCount());
        assertTrue(walker.resumeTask(task));
        assertEquals(1, walker.getTaskCount());
        assertTrue(walker.doTask().isPresent());
    }

    @Test
    public void testResumeSuspendedTask() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.suspend()).build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        assertEquals(Task.Status.SUSPENDED, task.getStatus());
        assertEquals(0, walker.getTaskCount());
        assertTrue(walker.resumeTask(task));
        assertFalse(walker.resumeTask(task));
        assertEquals(1, walker.getTaskCount());
        assertTrue(walker.doTask().isPresent());
        assertFalse(walker.hasTask());
    }

    @Test
    public void testResumeCompleteTask() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.complete()).build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        assertEquals(Task.Status.COMPLETED, task.getStatus());
        assertEquals(0, walker.getTaskCount());
        assertFalse(walker.resumeTask(task));
        assertEquals(0, walker.getTaskCount());
    }

    @Test
    public void testPreviousState() {
        var s1 = StateBuilder.newBuilder().setCode("A").setOnState(t -> State.Result.stay()).build();
        var walker = WalkerBuilder.newBuilder().addStates(s1).build();
        var task = walker.addTask(null, "A");
        walker.doTask();
        assertTrue(task.getPreviousState().isEmpty());
        walker.doTask();
        assertEquals("A", task.getPreviousState().get());
    }

    @Test
    public void testTaskSleeping() {
        var s1 = StateBuilder.<Counter>newBuilder().setCode("A").setOnState(t -> {
            t.getContent().increase();
            if (t.getContent().getCount() < 2) {
                return State.Result.sleep(-1000);
            }
            if (t.getContent().getCount() < 3) {
                return State.Result.sleep(1000);
            }
            return State.Result.complete();
        }).build();
        var walker = WalkerBuilder.<Counter>newBuilder().addStates(s1).build();
        var t1 = walker.addTask(new Counter(), "A");
        var t2 = walker.addTask(new Counter(), "A");
        while (t1.getStatus() != Task.Status.COMPLETED) {
            walker.doTask();
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException ex) {
            }
        }
        assertTrue(walker.hasTask());
        assertEquals(3, t1.getContent().getCount());
        assertEquals(Task.Status.SLEEPING, t2.getStatus());
    }

    @Test
    public void testByImplementation() {
        var walker = WalkerBuilder.<Counter>newBuilder()
            .addStates(new StateA(), new StateB())
            .addTransitions(new Transition1(), new Transition2())
            .setEnableActionTracing(true)
            .build();

        var task = walker.addTask(new Counter(), "StateA");
        walker.getTasks().forEach(System.out::println);
        while (walker.hasTask()) {
            var t = walker.doTask().get();
            System.out.println(t);
            t.getActions().forEach(a -> {
                assertEquals(a.getName(), "onState");
                assertEquals(a.getProvider(), "StateA");
                assertTrue(a.getStartTime() <= a.getEndTime().get());
            });
        }
        assertEquals(Task.Status.COMPLETED, task.getStatus());
        assertEquals("StateB", task.getState());
        assertEquals(4, task.getContent().getCount());
    }

    @Test
    public void testByDeclaration() {
        var walker = WalkerBuilder.<Counter>newBuilder()
            .addState("StateA",
                t -> {
                    System.out.println("StateA | " + t);
                    t.getContent().increase();
                    if (t.getContent().getCount() < 2) {
                        return State.Result.stay();
                    }
                    return State.Result.exit("DONE");
                },
                t -> System.out.println("After Exit StateA | " + t),
                t -> System.out.println("Before Enter StateA | " + t)
            )
            .addState("StateB",
                t -> {
                    System.out.println("StateB | " + t);
                    var counter = t.getContent();
                    if (counter.getCount() > 3) {
                        return State.Result.complete();
                    }
                    t.getContent().increase();
                    return State.Result.exit("DONE");
                }
            )
            .addTransition("DONE", "StateA", "StateB", t -> System.out.println("Transition1 | " + t))
            .addTransition("DONE", "StateB", "StateA")
            .build();

        var task = walker.addTask(new Counter(), "StateA");
        while (walker.hasTask()) {
            walker.doTask();
        }
        assertEquals(Task.Status.COMPLETED, task.getStatus());
        assertEquals("StateB", task.getState());
        assertEquals(4, task.getContent().getCount());
    }

    @Test
    public void testTaskListener() {
        var listener = new TaskListener<Flag>() {
            @Override
            public void onComplete(Task<Flag> task) {
                task.getContent().setFlag(1);
            }

            @Override
            public void onSuspend(Task<Flag> task) {
                TaskListener.super.onSuspend(task);
                task.getContent().setFlag(2);
            }

            @Override
            public void onSleep(Task<Flag> task) {
                TaskListener.super.onSleep(task);
                task.getContent().setFlag(3);
            }

            @Override
            public void onError(Task<Flag> task) {
                TaskListener.super.onError(task);
                task.getContent().setFlag(4);
            }
        };
        var walker = WalkerBuilder.<Flag>newBuilder()
            .addState("A", t -> State.Result.complete())
            .addState("B", t -> State.Result.suspend())
            .addState("C", t -> State.Result.sleep(5000))
            .addListeners(listener, listener)
            .build();
        // Complete
        var task = walker.addTask(new Flag(), "A");
        walker.doTask();
        assertEquals(1, task.getContent().getFlag());
        // Suspend
        task = walker.addTask(new Flag(), "B");
        walker.doTask();
        assertEquals(2, task.getContent().getFlag());
        // Sleep
        task = walker.addTask(new Flag(), "C");
        walker.doTask();
        assertEquals(3, task.getContent().getFlag());
        // Error
        task = walker.addTask(new Flag(), "D");
        walker.doTask();
        assertEquals(4, task.getContent().getFlag());
    }

    public static class Counter implements Content {

        private int count;

        public void increase() {
            count++;
        }

        public int getCount() {
            return count;
        }

        @Override
        public String toString() {
            return "Counter{" + "count=" + count + '}';
        }
    }

    public static class StateA implements State<Counter> {

        @Action.Traced
        @Override
        public State.Result onState(Task<Counter> task) {
            task.getContent().increase();
            if (task.getContent().getCount() < 2) {
                return State.Result.stay();
            }
            return State.Result.exit("Transition1");
        }

        @Override
        public void onEnterState(Task<Counter> task) {
            System.out.println("Before Enter StateA | " + task);
        }

        @Override
        public void onExitState(Task<Counter> task) {
            System.out.println("After Exit StateA | " + task);
        }
    }

    public static class StateB implements State<Counter> {

        @Override
        public State.Result onState(Task<Counter> task) {
            var counter = task.getContent();
            if (counter.getCount() > 3) {
                return State.Result.complete();
            }
            task.getContent().increase();
            return State.Result.exit("Transition2");
        }
    }

    public static class Transition1 implements Transition<Counter> {

        @Override
        public String fromState() {
            return "StateA";
        }

        @Override
        public String toState() {
            return "StateB";
        }

        @Override
        public void onTransition(Task<Counter> task) {
            System.out.println("Transition1 | " + task);
        }
    }

    public static class Transition2 implements Transition<Counter> {

        @Override
        public String fromState() {
            return "StateB";
        }

        @Override
        public String toState() {
            return "StateA";
        }
    }

    public static class Flag implements Content {

        private int flag;

        public int getFlag() {
            return flag;
        }

        public void setFlag(int flag) {
            this.flag = flag;
        }
    }
}
