/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * The builder of {@code Walker}.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.2
 */
public class WalkerBuilder {

    /**
     * Create a new builder of {@code Walker}.
     *
     * @param <C> the content type
     * @return the builder
     */
    public static <C extends Content> Walker.Builder<C> newBuilder() {
        return new ConcreteBuilder<>();
    }

    /**
     * For static method only.
     */
    private WalkerBuilder() {
    }

    /**
     * The implementation of {@code Builder}.
     *
     * @param <C> the content type of tasks
     * @version 0.4
     * @since 0.1
     */
    private static final class ConcreteBuilder<C extends Content> implements Walker.Builder<C> {

        private final List<State<C>> states = new ArrayList<>();

        private final List<Transition<C>> transitions = new ArrayList<>();

        /**
         * @since 0.2
         */
        private final List<TaskListener<C>> listeners = new ArrayList<>();

        private boolean enableActionTracing;

        @Override
        public Walker.Builder<C> addState(String code, Function<Task<C>, State.Result> onState) {
            return addState(code, onState, null, null);
        }

        @Override
        public Walker.Builder<C> addState(String code, Function<Task<C>, State.Result> onState, Consumer<Task<C>> onEnterState, Consumer<Task<C>> onExitState) {
            states.add(StateBuilder.<C>newBuilder()
                .setCode(code)
                .setOnState(onState)
                .setOnEnterState(onEnterState)
                .setOnExitState(onExitState)
                .build()
            );
            return this;
        }

        @Override
        public Walker.Builder<C> addStates(State<C>... states) {
            this.states.addAll(List.of(states));
            return this;
        }

        @Override
        public Walker.Builder<C> addTransition(String code, String fromState, String toState) {
            return addTransition(code, fromState, toState, null);
        }

        @Override
        public Walker.Builder<C> addTransition(String code, String fromState, String toState, Consumer<Task<C>> onTransition) {
            transitions.add(TransitionBuilder.<C>newBuilder()
                .setCode(code)
                .setFromState(fromState)
                .setToState(toState)
                .setOnTransition(onTransition)
                .build()
            );
            return this;
        }

        @Override
        public Walker.Builder<C> addTransitions(Transition<C>... transitions) {
            this.transitions.addAll(List.of(transitions));
            return this;
        }

        @Override
        public Walker.Builder<C> addListeners(TaskListener<C>... listeners) {
            this.listeners.addAll(List.of(listeners));
            return this;
        }

        @Override
        public Walker.Builder<C> setEnableActionTracing(boolean enableActionTracing) {
            this.enableActionTracing = enableActionTracing;
            return this;
        }

        @Override
        public Walker<C> build() {
            if (states.isEmpty()) {
                throw new IllegalArgumentException("State is required");
            }
            var walker = new Walker<C>(enableActionTracing);
            states.forEach(walker::addState);
            transitions.forEach(walker::addTransition);
            listeners.forEach(walker::addListener);
            return walker;
        }
    }
}
