/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * A task to be execute.
 *
 * @param <C> the content of {@code Task}
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.1
 */
public class Task<C extends Content> implements Comparable<Task<C>> {

    /**
     * The status of this task.
     */
    private Status status = Status.INIT;

    /**
     * The code of current state.
     */
    private String state;

    /**
     * The code of previous state.
     */
    private String previousState;

    /**
     * The code of upcoming transition.
     */
    private String transition;

    /**
     * The content of this task.
     */
    private final C content;

    /**
     * The action tracing data.
     */
    private final List<Action> actions = new ArrayList<>();

    /**
     * The cause makes this task status be error.
     */
    private Throwable errorCause;

    /**
     * The time this task should wake up.
     *
     * @since 0.3
     */
    private long wakeupTime;

    /**
     * Create a task.
     *
     * @param content the content
     * @param state the initial state code
     */
    Task(C content, String state) {
        this.content = content;
        this.state = state;
    }

    /**
     * The current status.
     *
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Get content.
     *
     * @return the content
     */
    public C getContent() {
        return content;
    }

    /**
     * Get current state.
     *
     * @return the state code
     */
    public String getState() {
        return state;
    }

    /**
     * Get previous state.
     *
     * @return the code of previous state
     */
    public Optional<String> getPreviousState() {
        return Optional.ofNullable(previousState);
    }

    /**
     * Get the upcoming transition.
     *
     * @return the transition code, empty if no transition
     */
    public Optional<String> getTransition() {
        return Optional.ofNullable(transition);
    }

    /**
     * Get a read-only action tracing data of this task.
     *
     * @return empty if action tracing diabled
     */
    public List<Action> getActions() {
        return List.copyOf(actions);
    }

    /**
     * Get the error cause.
     *
     * @return empty if no error
     */
    public Optional<Throwable> getErrorCause() {
        return Optional.ofNullable(errorCause);
    }

    /**
     * Get the wake up time.
     *
     * @return wake up time
     * @since 0.3
     */
    public long getWakeupTime() {
        return wakeupTime;
    }

    /**
     * Start a execution process.
     */
    void executionStart() {
        actions.clear();
        errorCause = null;
        wakeupTime = 0L;
        // REMIND[Arren Ping|20200322] suspended|error -> processing.
        switch (status) {
            case SUSPENDED:
            case SLEEPING:
            case ERROR:
                status = Status.PROCESSING;
            default:
        }
    }

    /**
     * End a execution process.
     *
     * @param state current state code
     * @param transition_ the upcoming transition code, empty if no transition.
     */
    void executionEnd(String state, Optional<String> transition_) {
        // REMIND[Arren Ping|20200321] init -> processing when complete entry state execution process.
        if (status == Task.Status.INIT) {
            // REMIND[Arren Ping|20200320] init means no previous state
            status = Status.PROCESSING;
        } else {
            previousState = this.state;
        }
        this.state = state;
        this.transition = transition_.orElse(null);
    }

    /**
     * End a execution process - the task is suspended.
     *
     * @param state current state code
     * @since 0.3
     */
    void executionSuspend(String state) {
        executionEnd(state, Optional.empty());
        status = Status.SUSPENDED;
    }

    /**
     * End a execution process - the task goes to sleep.
     *
     * @param state current state code
     * @param sleepTime sleep time in milliseconds
     * @since 0.3
     */
    void executionSleep(String state, long sleepTime) {
        executionEnd(state, Optional.empty());
        wakeupTime = System.currentTimeMillis() + sleepTime;
        status = Status.SLEEPING;
    }

    /**
     * End a execution process - the task raised an error.
     *
     * @param cause the reason
     */
    void executionError(Throwable cause) {
        status = Status.ERROR;
        errorCause = cause;
    }

    /**
     * End a execution process - the task is completed.
     *
     * @param state current state code
     * @since 0.3 renamed from complete
     */
    void executionComplete(String state) {
        previousState = this.state;
        this.state = state;
        status = Status.COMPLETED;
        transition = null;
    }

    /**
     * Record new action.
     *
     * @param name action name
     * @param provider action provider name
     */
    void actionStart(String name, String provider) {
        var action = new Action(name, provider);
        actions.add(action);
    }

    /**
     * End latest action if not end.
     */
    void actionEnd() {
        actions.get(actions.size() - 1).end();
    }

    @Override
    public String toString() {
        return "Task{" + "status=" + status + ", state=" + state + ", previousState=" + previousState + ", transition=" + transition + ", content=" + content + ", actions=" + actions + ", errorCause=" + errorCause + '}';
    }

    /**
     * @since 0.3
     */
    @Override
    public int compareTo(Task<C> o) {
        return Long.compare(wakeupTime, o.wakeupTime);
    }

    /**
     * The status of a task.
     */
    public enum Status {
        /**
         * Task is initializing.
         * Task will be kept in this status before entry state execution process completed.
         */
        INIT,
        /**
         * Task is processing.
         */
        PROCESSING,
        /**
         * Task is sleeping.
         *
         * @since 0.3
         */
        SLEEPING,
        /**
         * Task is suspended.
         */
        SUSPENDED,
        /**
         * Task is completed.
         */
        COMPLETED,
        /**
         * Task was end with error.
         */
        ERROR;
    }
}
