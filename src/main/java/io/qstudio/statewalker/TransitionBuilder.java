/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import io.qstudio.statewalker.Transition.Builder;

import java.util.Objects;
import java.util.function.Consumer;

/**
 * The builder of {@code Transition}.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public class TransitionBuilder {

    /**
     * Create a new builder of {@code Transition}.
     *
     * @param <C> the content type
     * @return the builder
     */
    public static <C extends Content> Builder<C> newBuilder() {
        return new ConcreteBuilder<>();
    }

    /**
     * For static method only.
     */
    private TransitionBuilder() {
    }

    /**
     * The implementation of {@code Builder}.
     *
     * @param <C> the content type of tasks
     */
    private static final class ConcreteBuilder<C extends Content> implements Builder<C> {

        /**
         * The transition to build.
         */
        private final ConcreteTransition<C> transition = new ConcreteTransition<>();

        @Override
        public Builder<C> setCode(String code) {
            transition.code = code;
            return this;
        }

        @Override
        public Builder<C> setFromState(String code) {
            transition.fromState = code;
            return this;
        }

        @Override
        public Builder<C> setToState(String code) {
            transition.toState = code;
            return this;
        }

        @Override
        public Builder<C> setOnTransition(Consumer<Task<C>> onTransition) {
            transition.onTransition = onTransition;
            return this;
        }

        @Override
        public Transition<C> build() {
            Objects.requireNonNull(transition.code, "code is required");
            Objects.requireNonNull(transition.fromState, "fromState is required");
            Objects.requireNonNull(transition.toState, "toState is required");
            return transition;
        }
    }

    /**
     * The implementation of {@code Transition} for {@code ConcreteBuilder} building new {@code Transition}.
     *
     * @param <C> the content type of tasks
     */
    private static final class ConcreteTransition<C extends Content> implements Transition<C> {

        /**
         * The code of this transition.
         */
        private String code;

        /**
         * The from state code of this transition.
         */
        private String fromState;

        /**
         * The to state code of this transition.
         */
        private String toState;

        /**
         * The {@code onTransition} of this transition.
         */
        private Consumer<Task<C>> onTransition;

        @Override
        public String getCode() {
            return code;
        }

        @Override
        public String fromState() {
            return fromState;
        }

        @Override
        public String toState() {
            return toState;
        }

        @Override
        public void onTransition(Task<C> task) {
            if (onTransition != null) {
                onTransition.accept(task);
            }
        }
    }
}
