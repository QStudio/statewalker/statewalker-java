/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

/**
 * Task listener using to monitor task status.
 *
 * @param <C> the content of {@code Task}
 * @author Arren Ping at QStudio.io
 * @version 0.3
 * @since 0.2
 */
@FunctionalInterface
public interface TaskListener<C extends Content> {

    /**
     * Invoke after the task completed.
     *
     * @param task the task which is completed
     */
    void onComplete(Task<C> task);

    /**
     * Invoke after the task raised an error.
     *
     * @param task the task which raised an error
     */
    default void onError(Task<C> task) {
    }

    /**
     * Invoke after the task is suspended.
     *
     * @param task the task which is suspended
     */
    default void onSuspend(Task<C> task) {
    }

    /**
     * Invoke after the task goes to sleep.
     *
     * @param task the task which is sleeping
     * @since 0.3
     */
    default void onSleep(Task<C> task) {
    }
}
