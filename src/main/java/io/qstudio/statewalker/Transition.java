/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.function.Consumer;

/**
 * The path from state one to state two.
 *
 * @param <C> the content of {@code Task}
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public interface Transition<C extends Content> {

    /**
     * The code of this transition, must be a constant value and unique to it's fromState.
     *
     * @return code of this transition
     * @since 0.4 definition changed
     */
    default String getCode() {
        return this.getClass().getSimpleName();
    }

    /**
     * From which state, must be a constant value.
     *
     * @return the state code this transition comes from
     */
    String fromState();

    /**
     * To which state, must be a constant value.
     *
     * @return the state code this transition goes to
     */
    String toState();

    /**
     * Run specific logic of this transition.
     *
     * @param task the task to be executed
     */
    default void onTransition(Task<C> task) {
    }

    /**
     * The builder of {@code Transition}.
     *
     * @param <C> the content of {@code Task}
     */
    interface Builder<C extends Content> {

        /**
         * Set the code for the transition.
         * This code must be set.
         *
         * @param code the transition code
         * @return this builder
         * @since 0.4 change to required
         */
        Builder<C> setCode(String code);

        /**
         * Set the code for the from state.
         * This code must be set.
         *
         * @param code the from state code
         * @return this builder
         */
        Builder<C> setFromState(String code);

        /**
         * Set the code for the to state.
         * This code must be set.
         *
         * @param code the to state code
         * @return this builder
         */
        Builder<C> setToState(String code);

        /**
         * Set the {@code onTransition} method for the transition.
         * The {@code onTransition} is optional.
         *
         * @param onTransition the {@code onTransition} logic of the transition
         * @return this builder
         */
        Builder<C> setOnTransition(Consumer<Task<C>> onTransition);

        /**
         * Build the transition.
         * If any must be set property are null, build will be fail and return empty.
         *
         * @return the transition
         * @throws NullPointerException if any necessary property not provided
         * @since 0.2 using NullPointerException instead of Optional
         */
        Transition<C> build();
    }
}
