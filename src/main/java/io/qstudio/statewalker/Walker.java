/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import io.qstudio.statewalker.State.Result;
import io.qstudio.statewalker.exception.StateNotFoundException;
import io.qstudio.statewalker.exception.TransitionNotFoundException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * The walker executing tasks based on the predefined states and transitions.
 * The task will be executed from state to state till completed.
 *
 * @param <C> the content type of tasks
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public class Walker<C extends Content> {

    /**
     * @since 0.4
     */
    private static final LifeCycleRunner NORMAL_RUNNER = new NormalLifeCycleRunner();

    /**
     * @since 0.4
     */
    private static final LifeCycleRunner TRACED_RUNNER = new TracedLifeCycleRunner();

    /**
     * Clone a walker.
     * Reuse states, transitions, listeners and runner.
     *
     * @param <C> the content type of tasks
     * @param walker the source walker
     * @return new walker
     * @since 0.2
     */
    static <C extends Content> Walker<C> clone(Walker<C> walker) {
        return new Walker<>(walker.states, walker.transitions, walker.listeners, walker.runner);
    }

    /**
     * K is the state code, V is the state.
     */
    private final Map<String, State<C>> states;

    /**
     * K is the state code, V is the transition map which K is transition code and V is the transition.
     *
     * @since 0.4 redesign to state code -> transition code -> transition
     */
    private final Map<String, Map<String, Transition<C>>> transitions;

    /**
     * The task queue.
     */
    private final Queue<Task<C>> tasks = new ConcurrentLinkedQueue<>();

    /**
     * The tasks which is processing.
     *
     * @since 0.2
     */
    private final List<Task<C>> processingTasks = new CopyOnWriteArrayList<>();

    /**
     * The tasks are sleeping.
     *
     * @since 0.3
     */
    private final Queue<Task<C>> sleepingTasks = new PriorityBlockingQueue<>();

    /**
     * The runner to run life cycle.
     */
    private final LifeCycleRunner runner;

    /**
     * Listeners for tasks.
     *
     * @since 0.2
     */
    private final List<TaskListener<C>> listeners;

    /**
     * Create a walker.
     *
     * @param enableActionTracing enable action tracing
     */
    Walker(boolean enableActionTracing) {
        this(new TreeMap<>(), new TreeMap<>(), new ArrayList<>(), enableActionTracing ? TRACED_RUNNER : NORMAL_RUNNER);
    }

    /**
     * Create a walker.
     *
     * @param states the state map
     * @param transitions the transition map
     * @param listeners the listener list
     * @param runner the runner
     * @since 0.4 redesign the signature
     * @since 0.2
     */
    private Walker(Map<String, State<C>> states, Map<String, Map<String, Transition<C>>> transitions, List<TaskListener<C>> listeners, LifeCycleRunner runner) {
        this.states = states;
        this.transitions = transitions;
        this.listeners = listeners;
        this.runner = runner;
    }

    /* ====================
     * Public Methods
     * ==================== */
    /**
     * Has task or not.
     *
     * @return true if tasks not completed.
     */
    public boolean hasTask() {
        return !tasks.isEmpty() || !sleepingTasks.isEmpty();
    }

    /**
     * Add a new task by content and initial state.
     *
     * @param content the content of the task
     * @param state the initial state of the task
     * @return the task added
     */
    public Task<C> addTask(C content, String state) {
        var t = new Task<C>(content, state);
        tasks.add(t);
        return t;
    }

    /**
     * Resume a suspended or error task.
     *
     * @param task a suspended or error task.
     * @return resume success or not
     */
    public boolean resumeTask(Task<C> task) {
        if ((task.getStatus() == Task.Status.SUSPENDED || task.getStatus() == Task.Status.ERROR) && !tasks.contains(task)) {
            return tasks.add(task);
        }
        return false;
    }

    /**
     * Get all tasks.
     *
     * @return task list
     */
    public List<Task<C>> getTasks() {
        var result = new ArrayList<Task<C>>(tasks);
        result.addAll(sleepingTasks);
        return List.copyOf(result);
    }

    /**
     * Get task count.
     *
     * @return the count of task queue.
     * @since 0.2
     */
    public int getTaskCount() {
        return tasks.size() + sleepingTasks.size();
    }

    /**
     * Do a task.
     * This method performs the task by transit it's state to next state.
     *
     * @return empty if no task.
     */
    public Optional<Task<C>> doTask() {
        wakeTasks();
        var t = tasks.poll();
        // REMIND[Arren Ping|20200423] there is a really small possibility that the task is neither in tasks nor in the processingTasks.
        if (t == null) {
            return Optional.empty();
        }
        processingTasks.add(t);
        try {
            execute(t);
        } catch (RuntimeException ex) {
            t.executionError(ex);
            listeners.forEach(l -> l.onError(t));
        }
        processingTasks.remove(t);
        return Optional.of(t);
    }

    /**
     * This walker is processing tasks or not.
     *
     * @return is processing or not
     * @since 0.2
     */
    public boolean isProcessing() {
        return !processingTasks.isEmpty();
    }

    /* ====================
     * Methods for Builder
     * ==================== */
    /**
     * Add a state to this walker.
     *
     * @param state the state to be added
     */
    void addState(State<C> state) {
        states.put(state.getCode(), state);
    }

    /**
     * Add a transition to this walker.
     *
     * @param transition the transition to be added
     */
    void addTransition(Transition<C> transition) {
        transitions.computeIfAbsent(transition.fromState(), k -> new TreeMap<>())
            .put(transition.getCode(), transition);
    }

    /**
     * Add a task listener to this walker.
     *
     * @param listener the listener to be added
     * @since 0.2
     */
    void addListener(TaskListener<C> listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    /* ====================
     * Main Logic Methods
     * ==================== */
    /**
     * Execute a task.
     *
     * @param task the task
     */
    private void execute(Task<C> task) {
        /*
         * LOGIC[Arren Ping|20200519] task execute logic
         * - 0.3|20200519 by Arren Ping
         * - 0.1|20200322 by Arren Ping
         * - 0.1|20200320 by Arren Ping
         * - 0.1|20200319 by Arren Ping
         * ----------------------------------------
         * 0. start task execution
         * 1. get current state
         * 2. get transition and next state
         * 2.1. no transition => stay current state
         * 2.2. with transition => get next state
         * 3. transition process if current state != next state
         * 3.1. run on exit current state logic
         * 3.2. run on transition logic
         * 3.3. run on enter next state logic
         * 4. run state
         * 5. check result feedback and record task
         * 5.1. continue => re-offer task
         * 5.2. suspend => invoke onSuspend
         * 5.3. sleep => add to sleep queue and invoke onSleep
         * 5.4. complete => invoke onComplete
         */
        // STEP[0]
        task.executionStart();
        // STEP[1]
        var currentState = getState(task.getState());
        // STEP[2]
        var transition_ = getTransition(task.getState(), task.getTransition());
        var nextState = getNextState(task.getState(), transition_);
        // STEP[3 & 4]
        var result = runner.run(currentState, transition_, nextState, task);
        // STEP[5]
        switch (result.getFeedback()) {
            // STEP[5.1]
            case CONTINUE:
                processContinue(task, nextState.getCode(), result.getExitCode());
                break;
            // STEP[5.2]
            case SUSPEND:
                processSuspend(task, nextState.getCode());
                break;
            // STEP[5.3]
            case SLEEP:
                processSleep(task, nextState.getCode(), result.getSleepTime());
                break;
            // STEP[5.4]
            case COMPLETE:
            default:
                processComplete(task, nextState.getCode());
                break;
        }
    }

    /**
     * Get state.
     *
     * @param code the state code
     * @return the state
     */
    private State<C> getState(String code) {
        var state = states.get(code);
        if (state == null) {
            throw new StateNotFoundException(code);
        }
        return state;
    }

    /**
     * Get transition.
     *
     * @param state current state code
     * @param transition_ the next transition code
     * @return the transition
     * @since 0.4 redesign the signature
     */
    private Optional<Transition<C>> getTransition(String state, Optional<String> transition_) {
        if (transition_.isEmpty()) {
            return Optional.empty();
        }
        var transition = transitions.getOrDefault(state, Collections.emptyMap()).get(transition_.get());
        // REMIND[Arren Ping|20200319] ensure transition is existed
        if (transition == null) {
            throw new TransitionNotFoundException(transition_.get());
        }
        return Optional.of(transition);
    }

    /**
     * Get next state.
     *
     * @param currentState current state code
     * @param transition_ the transition
     * @return next state
     */
    private State<C> getNextState(String currentState, Optional<Transition<C>> transition_) {
        if (transition_.isEmpty()) {
            return getState(currentState);
        }
        return getState(transition_.get().toState());
    }

    /**
     * Wake up tasks.
     *
     * @since 0.3
     */
    private void wakeTasks() {
        long now = System.currentTimeMillis();
        var s = sleepingTasks.peek();
        while (s != null && now >= s.getWakeupTime()) {
            tasks.add(sleepingTasks.poll());
            s = sleepingTasks.peek();
        }
    }

    /**
     * Process the continued task.
     *
     * @param task the task
     * @param currentState the code of current state
     * @param nextTransition_ the code of next transition
     * @since 0.4 redesign the signature
     * @since 0.3
     */
    private void processContinue(Task<C> task, String currentState, Optional<String> nextTransition_) {
        task.executionEnd(currentState, nextTransition_);
        tasks.offer(task);
    }

    /**
     * Process the suspended task.
     *
     * @param task the task
     * @param currentState the code of current state
     * @since 0.3
     */
    private void processSuspend(Task<C> task, String currentState) {
        task.executionSuspend(currentState);
        listeners.forEach(l -> l.onSuspend(task));
    }

    /**
     * Process the task went to sleep.
     *
     * @param task the task
     * @param currentState the code of current state
     * @param sleepTime sleep time in milliseconds
     * @since 0.3
     */
    private void processSleep(Task<C> task, String currentState, long sleepTime) {
        task.executionSleep(currentState, sleepTime);
        sleepingTasks.offer(task);
        listeners.forEach(l -> l.onSleep(task));
    }

    /**
     * Process the completed task.
     *
     * @param task the task
     * @param currentState the code of current state
     * @since 0.3
     */
    private void processComplete(Task<C> task, String currentState) {
        task.executionComplete(currentState);
        listeners.forEach(l -> l.onComplete(task));
    }

    /**
     * The builder of {@code Walker}.
     *
     * @param <C> the content type of tasks
     */
    public interface Builder<C extends Content> {

        /**
         * Add a state to the walker.
         *
         * @param code the state code
         * @param onState the {@code onState} logic of the state
         * @return this builder
         * @since 0.2
         */
        Builder<C> addState(String code, Function<Task<C>, Result> onState);

        /**
         * Add a state to the walker.
         *
         * @param code the state code
         * @param onState the {@code onState} logic of the state
         * @param onEnterState the {@code onEnterState} logic of the state
         * @param onExitState the {@code onExitState} logic of the state
         * @return this builder
         * @since 0.2
         */
        Builder<C> addState(String code, Function<Task<C>, Result> onState, Consumer<Task<C>> onEnterState, Consumer<Task<C>> onExitState);

        /**
         * Add states to the walker.
         *
         * @param states the states the walker should know
         * @return this builder
         */
        Builder<C> addStates(State<C>... states);

        /**
         * Add a transition to the walker.
         *
         * @param code the transition code
         * @param fromState the from state code
         * @param toState the to state code
         * @return this builder
         * @since 0.4 code changes to required
         * @since 0.2
         */
        Builder<C> addTransition(String code, String fromState, String toState);

        /**
         * Add a transition to the walker.
         *
         * @param code the transition code
         * @param fromState the from state code
         * @param toState the to state code
         * @param onTransition the {@code onTransition} logic of the transition
         * @return this builder
         * @since 0.4 code changes to required
         * @since 0.2
         */
        Builder<C> addTransition(String code, String fromState, String toState, Consumer<Task<C>> onTransition);

        /**
         * Add transitions to the walker.
         *
         * @param transitions the transitions the walker should know
         * @return this builder
         */
        Builder<C> addTransitions(Transition<C>... transitions);

        /**
         * Add listeners to the walker.
         *
         * @param listeners the task listeners to be added
         * @return this builder
         * @since 0.2
         */
        Builder<C> addListeners(TaskListener<C>... listeners);

        /**
         * Set enable action tracing or not.
         *
         * @param enableActionTracing enable action tracing
         * @return this builder
         */
        Builder<C> setEnableActionTracing(boolean enableActionTracing);

        /**
         * Build the walker.
         * At least, this walker should contain one state.
         *
         * @throws IllegalArgumentException if contains no state
         * @return the walker
         * @since 0.4 check the existence of state
         */
        Walker<C> build();
    }

    /**
     * The runner to run life cycle.
     *
     * @since 0.4 re-design the generic declaration
     */
    private interface LifeCycleRunner {

        /**
         * Run the life cycle.
         * <pre>
         * 1. if {@code currentState} is not {@code nextState}
         * 1.1. run {@code onExitState} of {@code currentState}
         * 1.2. run {@code onTransition} of {@code transition}
         * 1.3. run {@code onEnterState} of {@code nextState}
         * 2. run {@code onState} of {@code nextState}
         * </pre>
         *
         * @param currentState current state of the task
         * @param transition_ upcoming transition of the task
         * @param nextState next state of the task
         * @param task the task
         * @return the result of {@code onState}
         */
        <C extends Content> Result run(State<C> currentState, Optional<Transition<C>> transition_, State<C> nextState, Task<C> task);
    }

    /**
     * Run life cycle without action tracing.
     */
    private static final class NormalLifeCycleRunner implements LifeCycleRunner {

        @Override
        public <C extends Content> Result run(State<C> currentState, Optional<Transition<C>> transition_, State<C> nextState, Task<C> task) {
            if (transition_.isPresent()) {
                currentState.onExitState(task);
                transition_.get().onTransition(task);
                nextState.onEnterState(task);
            }
            return nextState.onState(task);
        }
    }

    /**
     * Run life cycle with action tracing.
     */
    private static final class TracedLifeCycleRunner implements LifeCycleRunner {

        @Override
        public <C extends Content> Result run(State<C> currentState, Optional<Transition<C>> transition_, State<C> nextState, Task<C> task) {
            if (transition_.isPresent()) {
                var transition = transition_.get();
                executeTracedLifeCycle(currentState, currentState.getCode(), "onExitState", task);
                executeTracedLifeCycle(transition, transition.getCode(), "onTransition", task);
                executeTracedLifeCycle(nextState, nextState.getCode(), "onEnterState", task);
            }
            return (Result) executeTracedLifeCycle(nextState, nextState.getCode(), "onState", task);
        }

        /**
         * Execute life cycle method with tracing.
         *
         * @param target target state or transition
         * @param targetCode the code of target
         * @param methodName which method to run
         * @param task the task
         * @return the result of target method
         */
        private Object executeTracedLifeCycle(Object target, String targetCode, String methodName, Task<? extends Content> task) {
            Object result = null;
            try {
                Method method = target.getClass().getMethod(methodName, Task.class);
                boolean traced = method.isAnnotationPresent(Action.Traced.class);
                if (traced) {
                    task.actionStart(methodName, targetCode);
                }
                result = method.invoke(target, task);
                if (traced) {
                    task.actionEnd();
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | RuntimeException ex) {
            }
            return result;
        }
    }
}
