/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * The runner runs walkers automatically and asynchronously.
 * A runner has a dedicated thread to command walkers to do task, and has a thread pool to execute tasks which the size defined by user.
 *
 * @param <C> the content type of tasks
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.2
 */
public class WalkerRunner<C extends Content> {

    /**
     * Thread sleep time per milliseconds.
     */
    private final static int SLEEP_TIME = 20;

    /**
     * @since 0.4
     */
    private final static int MIN_THREAD_POOL_SIZE = 1;

    /**
     * @since 0.4
     */
    private final static int MIN_GROUP_SIZE = 1;

    /**
     * The lock to prevent synchronous problem.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * K is the group code, V is the Group.
     * To user it is walker code.
     *
     * @since 0.4
     */
    private final Map<String, Group<C>> groups = new HashMap<>();

    /**
     * @since 0.4
     */
    private final int threadPoolSize;

    /**
     * This runner started or not.
     */
    private boolean started = false;

    /**
     * This runner is running or not.
     */
    private boolean running = false;

    /**
     * The executor to execute {@code Command} asynchronously.
     */
    private ExecutorService executor;

    /**
     * The main thread of this runner.
     */
    private Thread currentThread;

    /**
     * The logic of main thread.
     *
     * @since 0.4 name changed from runner to mainRunner
     */
    private final Runnable mainRunner = () -> {
        while (started) {
            var runners = groups.values().stream()
                .flatMap(g -> g.getRunners().stream())
                .collect(Collectors.toList());
            runners.forEach(executor::submit);
            try {
                TimeUnit.MILLISECONDS.sleep(SLEEP_TIME);
            } catch (InterruptedException ex) {
            }
        }
    };

    /**
     * Create a runner.
     *
     * @param walker the walker in this runner.
     * @param amount the amount of walkers in this runner
     * @param threadPoolSize the size of thread pool which minimum size is 1
     */
    WalkerRunner(List<Group<C>> groups, int threadPoolSize) {
        this.threadPoolSize = threadPoolSize > MIN_THREAD_POOL_SIZE ? threadPoolSize : MIN_THREAD_POOL_SIZE;
        groups.forEach(g -> this.groups.put(g.code, g));
    }

    /**
     * Assign task to runner.
     *
     * @param code the walker code
     * @param content the content of the task
     * @param state the initial state of the task
     * @return the task assigned
     * @since 0.4 redesign the signature
     */
    public Optional<Task<C>> assignTask(String code, C content, String state) {
        return Optional.ofNullable(groups.get(code))
            .map(g -> g.assignTask(content, state));
    }

    /**
     * Start runner.
     *
     * @return success start or not
     */
    public boolean start() {
        var result = false;
        if (!started) {
            lock.lock();
            try {
                if (!started) {
                    started = true;
                    executor = Executors.newFixedThreadPool(threadPoolSize);
                    currentThread = new Thread(mainRunner);
                    currentThread.start();
                    running = true;
                    result = true;
                }
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    /**
     * Stop runner.
     *
     * @return success stop or not
     */
    public boolean stop() {
        var result = false;
        if (started) {
            lock.lock();
            try {
                if (started) {
                    started = false;
                    wait(currentThread);
                    currentThread = null;
                    executor.shutdown();
                    executor = null;
                    running = false;
                    result = true;
                }
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    /**
     * This runner is busy or not.
     * Busy means the runner is running, has tasks to be completed.
     *
     * @return busy or not
     */
    public boolean isBusy() {
        if (!running) {
            return false;
        }
        return groups.values().stream().anyMatch(Group::isBusy);
    }

    /**
     * This runner is running or not.
     *
     * @return running or not
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Get the size of the thread pool belongs to this runner.
     *
     * @return the pool size
     * @since 0.4
     */
    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    /**
     * Get the walker codes of this runner.
     *
     * @return a set of code
     * @since 0.4
     */
    public Set<String> getWalkerCodes() {
        return Set.copyOf(groups.keySet());
    }

    /**
     * Wait till the completion of the thread.
     *
     * @param thread
     */
    private void wait(Thread thread) {
        try {
            thread.join();
        } catch (InterruptedException ex) {
        }
    }

    /**
     * WalkerRunner group.
     *
     * @since 0.4
     */
    static class Group<C extends Content> {

        /**
         * Group Code.
         */
        private final String code;

        /**
         * The walkers of this Group.
         */
        private final List<Walker<C>> walkers = new ArrayList<>();

        /**
         * K is the walker, V is the runnable per each walker.
         */
        private final Map<Walker<?>, Runnable> runners = new HashMap<>();

        Group(String code, Walker<C> walker, int walkerCount) {
            this.code = code;
            var count = walkerCount > MIN_GROUP_SIZE ? walkerCount : MIN_GROUP_SIZE;
            for (var i = 0; i < count; i++) {
                var w = Walker.clone(walker);
                walkers.add(w);
                runners.put(w, w::doTask);
            }
        }

        /**
         * Get runners to be executed.
         *
         * @return the runnable list
         * @since 0.4 refactor from WalkerRunner
         * @since 0.4 name changed from getCommands to getTaskRunners
         */
        private List<Runnable> getRunners() {
            return walkers.stream()
                .filter(Walker::hasTask)
                .map(runners::get)
                .collect(Collectors.toList());
        }

        /**
         * Assign task to runner.
         *
         * @param content the content of the task
         * @param state the initial state of the task
         * @return the task assigned
         * @since 0.4 refactor from WalkerRunner
         */
        private Task<C> assignTask(C content, String state) {
            return walkers.stream()
                .min(Comparator.comparing(Walker::getTaskCount))
                .get().addTask(content, state);
        }

        /**
         * This group is busy or not.
         * Busy means the walkers have any task to be completed.
         *
         * @return busy or not.
         */
        private boolean isBusy() {
            return walkers.stream().anyMatch(w -> (w.hasTask() || w.isProcessing()));
        }
    }

    /**
     * The builder of {@code WalkerRunner}.
     *
     * @param <C> the content type of tasks
     * @since 0.4 totally resign this interface
     */
    public interface Builder<C extends Content> {

        /**
         * Add a walker to the runner.
         *
         * @param code the code of walker
         * @param amount the amount of this walker should be created which minimum value is 1.
         * @param walker the walker to be added to this runner
         * @return this builder
         * @since 0.4
         */
        Builder<C> addWalker(String code, int amount, Walker<C> walker);

        /**
         * Set the thread pool size of this runner which minimum value is 1.
         *
         * @param size the thread pool size
         * @return this builder
         * @since 0.4
         */
        Builder<C> setThreadPoolSize(int size);

        /**
         * Build the walker.
         * At least, this runner should contains one walker.
         *
         * @throws IllegalArgumentException if contains no walker
         * @return the walker
         * @since 0.4 check the existence of walker
         */
        WalkerRunner<C> build();
    }
}
