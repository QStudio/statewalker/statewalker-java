/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

/**
 * The action records one step tracing data of a task single execution.
 */
public class Action {

    /**
     * Action name.
     * The method name, such as {@code onState}.
     */
    private final String name;

    /**
     * Provider name. The action provided by which state or transition.
     * It is the code of state or transition.
     */
    private final String provider;

    /**
     * Action start time.
     */
    private final Long startTime;

    /**
     * Action end time.
     */
    private Long endTime;

    /**
     * Create an action.
     *
     * @param name action name
     * @param provider action provider name
     */
    Action(String name, String provider) {
        this.name = name;
        this.provider = provider;
        startTime = System.currentTimeMillis();
    }

    /**
     * Get the action name.
     *
     * @return the action name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the provider name.
     *
     * @return the provider name
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Get action start time.
     *
     * @return the start time in milliseconds.
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * Get action end time.
     *
     * @return the end time in milliseconds.
     */
    public Optional<Long> getEndTime() {
        return Optional.ofNullable(endTime);
    }

    /**
     * Record end time.
     */
    void end() {
        endTime = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "Action{" + "name=" + name + ", provider=" + provider + ", startTime=" + startTime + ", endTime=" + endTime + '}';
    }

    /**
     * The life cycle method will be traced if add {@code Recordable}.
     *
     * @see State#onEnterState(io.qstudio.statewalker.Task)
     * @see State#onState(io.qstudio.statewalker.Task)
     * @see State#onExitState(io.qstudio.statewalker.Task)
     * @see Transition#onTransition(io.qstudio.statewalker.Task)
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    public @interface Traced {
    }
}
