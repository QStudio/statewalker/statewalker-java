/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import io.qstudio.statewalker.State.Builder;
import io.qstudio.statewalker.State.Result;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * The builder of {@code State}.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.2
 * @since 0.1
 */
public class StateBuilder {

    /**
     * Create a new builder of {@code State}.
     *
     * @param <C> the content type
     * @return the builder
     */
    public static <C extends Content> Builder<C> newBuilder() {
        return new ConcreteBuilder<>();
    }

    /**
     * For static method only.
     */
    private StateBuilder() {
    }

    /**
     * The implementation of {@code Builder}.
     *
     * @param <C> the content type of tasks
     */
    private static final class ConcreteBuilder<C extends Content> implements Builder<C> {

        /**
         * The state to build.
         */
        private final ConcreteState<C> state = new ConcreteState<>();

        @Override
        public Builder<C> setCode(String code) {
            state.code = code;
            return this;
        }

        @Override
        public Builder<C> setOnState(Function<Task<C>, Result> onState) {
            state.onState = onState;
            return this;
        }

        @Override
        public Builder<C> setOnEnterState(Consumer<Task<C>> onEnterState) {
            state.onEnterState = onEnterState;
            return this;
        }

        @Override
        public Builder<C> setOnExitState(Consumer<Task<C>> onExitState) {
            state.onExitState = onExitState;
            return this;
        }

        @Override
        public State<C> build() {
            Objects.requireNonNull(state.code, "code is required");
            Objects.requireNonNull(state.onState, "onState is required");
            return state;
        }
    }

    /**
     * The implementation of {@code State} for {@code ConcreteBuilder} building new {@code State}.
     *
     * @param <C> the content type of tasks
     */
    private static final class ConcreteState<C extends Content> implements State<C> {

        /**
         * The code of this state.
         */
        private String code;

        /**
         * The {@code onState} of this state.
         */
        private Function<Task<C>, Result> onState;

        /**
         * The {@code onEnterState} of this state.
         */
        private Consumer<Task<C>> onEnterState;

        /**
         * The {@code onExitState} of this state.
         */
        private Consumer<Task<C>> onExitState;

        @Override
        public String getCode() {
            return code;
        }

        @Override
        public Result onState(Task<C> task) {
            return onState.apply(task);
        }

        @Override
        public void onEnterState(Task<C> task) {
            if (onEnterState != null) {
                onEnterState.accept(task);
            }
        }

        @Override
        public void onExitState(Task<C> task) {
            if (onExitState != null) {
                onExitState.accept(task);
            }
        }
    }
}
