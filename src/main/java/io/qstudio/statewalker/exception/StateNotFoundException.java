/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker.exception;

/**
 * Thrown when walker can't find the state.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.1
 * @since 0.1
 */
public class StateNotFoundException extends RuntimeException {

    /**
     * Constructs an {@code StateNotExistedException}.
     *
     * @param stateCode the state code
     */
    public StateNotFoundException(String stateCode) {
        super("State[" + stateCode + "] is not found");
    }
}
