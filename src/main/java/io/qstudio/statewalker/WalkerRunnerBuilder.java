/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.ArrayList;
import java.util.List;

/**
 * The builder of {@code WalkerRunner}.
 *
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.2
 */
public class WalkerRunnerBuilder {

    /**
     * Create a new builder of {@code WalkerRunner}.
     *
     * @param <C> the content type
     * @return the builder
     */
    public static <C extends Content> WalkerRunner.Builder<C> newBuilder() {
        return new ConcreteBuilder<>();
    }

    /**
     * For static method only.
     */
    private WalkerRunnerBuilder() {
    }

    /**
     * The implementation of {@code Builder}.
     *
     * @param <C> the content type of tasks
     * @since 0.4 re-implement the new designed interface
     */
    private static final class ConcreteBuilder<C extends Content> implements WalkerRunner.Builder<C> {

        private final List<WalkerRunner.Group<C>> groups = new ArrayList<>();

        private int threadPoolSize = 1;

        @Override
        public WalkerRunner.Builder<C> addWalker(String code, int amount, Walker<C> walker) {
            groups.add(new WalkerRunner.Group<>(code, walker, amount));
            return this;
        }

        @Override
        public WalkerRunner.Builder<C> setThreadPoolSize(int size) {
            this.threadPoolSize = size;
            return this;
        }

        @Override
        public WalkerRunner<C> build() {
            if (groups.isEmpty()) {
                throw new IllegalArgumentException("Walker is required");
            }
            return new WalkerRunner<>(groups, threadPoolSize);
        }
    }
}
