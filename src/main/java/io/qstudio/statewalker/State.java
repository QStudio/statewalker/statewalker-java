/*
 * This source file is part of the QStudio.io open source project
 *
 * Copyright (C) 2020-present QStudio and the project authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
package io.qstudio.statewalker;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A step of executing a {@code Task}.
 * The life cycle method sequence is {@code onEnterState -> onState -> onExitState}.
 *
 * @param <C> the content of {@code Task}
 * @author Arren Ping at QStudio.io
 * @version 0.4
 * @since 0.1
 */
public interface State<C extends Content> {

    /**
     * Get the code of state which must be an unique constant value.
     *
     * @return the code of this state.
     */
    default String getCode() {
        return this.getClass().getSimpleName();
    }

    /**
     * Run specific logic of this state.
     *
     * @param task the task to be executed
     * @return the result after running logic
     */
    Result onState(Task<C> task);

    /**
     * Run specific logic on enter this state.
     *
     * @param task the task to be executed
     */
    default void onEnterState(Task<C> task) {
    }

    /**
     * Run specific logic on exit this state.
     *
     * @param task the task to be executed
     */
    default void onExitState(Task<C> task) {
    }

    /**
     * The result of running state logic.
     */
    class Result {

        /**
         * The result for stay.
         *
         * @since 0.3
         */
        private static final Result STAY = new Result(Feedback.CONTINUE);

        /**
         * The result for suspend.
         *
         * @since 0.3
         */
        private static final Result SUSPEND = new Result(Feedback.SUSPEND);

        /**
         * The result for complete.
         *
         * @since 0.3
         */
        private static final Result COMPLETE = new Result(Feedback.COMPLETE);

        /**
         * Generate a result to exit this state.
         *
         * @param exitCode the exit code
         * @return the result to exit this state
         * @since 0.4 nextState nenamed to exitCode
         */
        public static Result exit(String exitCode) {
            return new Result(Feedback.CONTINUE, exitCode);
        }

        /**
         * Stay on the same state.
         *
         * @return the result to stay on the same state
         */
        public static Result stay() {
            return STAY;
        }

        /**
         * Suspend and stay on the same state.
         * This means the first state will be the suspended one when task resumed.
         *
         * @return the result to suspend
         * @since 0.3 renamed from stayAndWait
         */
        public static Result suspend() {
            return SUSPEND;
        }

        /**
         * Generate a completed result.
         *
         * @return the result which the task is completed
         */
        public static Result complete() {
            return COMPLETE;
        }

        /**
         * Sleep for the specific time period.
         * If {@code sleepTime} less then 0L will be set to 0L.
         *
         * @param sleepTime sleep time in milliseconds
         * @return the result to sleep
         * @since 0.3
         */
        public static Result sleep(long sleepTime) {
            return new Result(Feedback.SLEEP, null, (sleepTime < 0L ? 0L : sleepTime));
        }

        /**
         * The feedback for walker.
         */
        private final Feedback feedback;

        /**
         * The exitCode for Walker to route to next state.
         * In current implementation, the exitCode should be the code of transitions from current state to next state.
         *
         * @since 0.4 renamed to exitCode
         */
        private final String exitCode;

        /**
         * Sleep time in milliseconds.
         *
         * @since 0.3
         */
        private final long sleepTime;

        /**
         * Create a {@code Result}.
         *
         * @param feedback the feedback
         * @param exitCode the exit code
         * @param sleepTime sleep time in milliseconds
         * @since 0.4 nextState nenamed to exitCode
         */
        private Result(Feedback feedback, String exitCode, long sleepTime) {
            this.feedback = feedback;
            this.exitCode = exitCode;
            this.sleepTime = sleepTime;
        }

        /**
         * Create a {@code Result}.
         *
         * @param feedback the feedback
         */
        private Result(Feedback feedback) {
            this(feedback, null, 0L);
        }

        /**
         * Create a {@code Result}.
         *
         * @param feedback the feedback
         * @param exitCode the exit code
         * @since 0.4 nextState nenamed to exitCode
         */
        private Result(Feedback feedback, String exitCode) {
            this(feedback, exitCode, 0L);
        }

        /**
         * Get the feedback for this task.
         *
         * @return the feedback
         */
        Feedback getFeedback() {
            return feedback;
        }

        /**
         * The code to route to exit state.
         *
         * @return empty if stay in the same state.
         */
        Optional<String> getExitCode() {
            return Optional.ofNullable(exitCode);
        }

        /**
         * Sleep time.
         *
         * @return the sleep time.
         */
        long getSleepTime() {
            return sleepTime;
        }

        /**
         * The feedback for walker.
         */
        enum Feedback {
            /**
             * Continue doing task.
             */
            CONTINUE,
            /**
             * Suspend task.
             */
            SUSPEND,
            /**
             * Task sleep.
             *
             * @since 0.3
             */
            SLEEP,
            /**
             * Complete task.
             */
            COMPLETE;
        }
    }

    /**
     * The builder of {@code State}.
     *
     * @param <C> the content type of tasks
     */
    interface Builder<C extends Content> {

        /**
         * Set the code for the state.
         * The code must be set.
         *
         * @param code the state code
         * @return this builder
         */
        Builder<C> setCode(String code);

        /**
         * Set the {@code onState} method for the state.
         * The {@code onState} must be set.
         *
         * @param onState the {@code onState} logic of the state
         * @return this builder
         */
        Builder<C> setOnState(Function<Task<C>, Result> onState);

        /**
         * Set the {@code onEnterState} method for the state.
         * The {@code onEnterState} is optional.
         *
         * @param onEnterState the {@code onEnterState} logic of the state
         * @return this builder
         */
        Builder<C> setOnEnterState(Consumer<Task<C>> onEnterState);

        /**
         * Set the {@code onExitState} method for the state.
         * The {@code onExitState} is optional.
         *
         * @param onExitState the {@code onExitState} logic of the state
         * @return this builder
         */
        Builder<C> setOnExitState(Consumer<Task<C>> onExitState);

        /**
         * Build the state.
         * If any must be set property are null, build will be fail and return empty.
         *
         * @return the state
         * @throws NullPointerException if any necessary property not provided
         * @since 0.2 using NullPointerException instead of Optional
         */
        State<C> build();
    }
}
